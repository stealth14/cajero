select * from AspNetUsers
select * from account
select * from deposit

/*vistaso a relacion cuenta de usuario, cuenta bancaria*/
select c.balance ,a.UserName,c.code from 
AspNetUsers a,
account c
where a.Id = c.customerid


/*PROCEDIMIENTOS*/

create procedure increasevalue
( @cash float(10), @customerid nvarchar(450))
as

	UPDATE account
	SET balance = balance + @cash
	WHERE customerid = @customerid; 
go

create procedure decreasevalue
( @cash float(10), @customerid nvarchar(450))
as

	UPDATE account
	SET balance = balance - @cash
	WHERE customerid = @customerid; 
go


/*TRIGGERS*/

--despues de depositar
CREATE TRIGGER on_deposit  
ON dbo.deposit  
AFTER INSERT   
AS 
	declare @cash float(10);
	declare @customerid nvarchar(450);

	set @cash = (select cash from inserted);
	set @customerid = (select customerid from inserted);

	exec increasevalue @cash,@customerid 
GO 

--despues de retirar
CREATE TRIGGER on_pullout  
ON dbo.pullout  
AFTER INSERT   
AS 
	declare @cash float(10);
	declare @customerid nvarchar(450);

	set @cash = (select cash from inserted);
	set @customerid = (select customerid from inserted);
	
	exec decreasevalue @cash,@customerid
GO  

--despues de transferir
CREATE TRIGGER on_transference  
ON dbo.transference  
AFTER INSERT   
AS 
	declare @cash float(10);
	declare @depositorid nvarchar(450);
	declare @code nvarchar(450);

	set @cash = (select cash from inserted);
	set @depositorid = (select depositorid from inserted);
	set @code = (select depositaryid from inserted);

	/*obtener asp id de beneficiario*/
	declare @depositaryid nvarchar(450);
	set @depositaryid=(select AspNetUsers.Id from AspNetUsers,account where AspNetUsers.Id=customerid and account.code=@code);
	

	/*agrega fondos al beneficiario del deposito*/
	UPDATE account
	SET balance = balance + @cash
	WHERE customerid = @depositaryid; 

	/*retira fondos al depositante*/
	UPDATE account
	SET balance = balance - @cash
	WHERE customerid = @depositorid;

GO  

--control de saldo

create trigger no_founds on dbo.account
for update
as 
	if (select balance from inserted)<0
	begin
/*funcion*/
raiserror('Saldo insuficiente para completar la transaccion',15,217)
		rollback transaction
		print @@ROWCOUNT
end;
