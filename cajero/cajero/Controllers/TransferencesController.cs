﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cajero.Models;


//herramientas de usuario autenticado
using Microsoft.AspNetCore.Identity;
using cajero.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace cajero.Controllers
{
    [Authorize]
    public class TransferencesController : Controller
    {

        //enlace con usuario
        private readonly UserManager<IdentityUser> _manager;

        private readonly cajeroContext _context;

        public TransferencesController(UserManager<IdentityUser> manager, cajeroContext context)
        {
            _context = context;
            //inicializa el manager de usuarios
            _manager = manager;
        }
        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }

        // GET: Transferences
        public async Task<IActionResult> Index()
        {

            //obtiene instancia de usuario actual
            var user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var transferences = from t in _context.Transference
                           select t;
            //aplica clausula where para filtrar purchases por propietario
            transferences = transferences.Where(t => t.Depositorid == user.Id);

            //envia lista filtrada para su renderizacion en "index.cshtml"
            return View(await transferences.AsNoTracking().ToListAsync());
        }

        // GET: Transferences/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transference = await _context.Transference
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transference == null)
            {
                return NotFound();
            }

            return View(transference);
        }

        // GET: Transferences/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Transferences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Cash,Date,Depositorid,Depositaryid")] Transference transference)
        {

            var user = await GetCurrentUser();
            if (ModelState.IsValid)
            {
                //enlaza orden con usuario actual
                transference.Depositorid = user.Id;


                //setea fecha 
                transference.Date = DateTime.Now;

                _context.Add(transference);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(transference);
        }

        // GET: Transferences/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transference = await _context.Transference.FindAsync(id);
            if (transference == null)
            {
                return NotFound();
            }
            return View(transference);
        }

        // POST: Transferences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Cash,Date,Depositorid,Depositaryid")] Transference transference)
        {
            if (id != transference.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transference);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransferenceExists(transference.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(transference);
        }

        // GET: Transferences/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transference = await _context.Transference
                .FirstOrDefaultAsync(m => m.Id == id);
            if (transference == null)
            {
                return NotFound();
            }

            return View(transference);
        }

        // POST: Transferences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transference = await _context.Transference.FindAsync(id);
            _context.Transference.Remove(transference);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransferenceExists(int id)
        {
            return _context.Transference.Any(e => e.Id == id);
        }
    }
}
