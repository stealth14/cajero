﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cajero.Models;


//herramientas del usuario logeado
using Microsoft.AspNetCore.Identity;
using cajero.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace cajero.Controllers
{
    [Authorize]
    public class PulloutsController : Controller
    {
        private readonly cajeroContext _context;

        //enlace con usuario
        private readonly UserManager<IdentityUser> _manager;

        public PulloutsController(UserManager<IdentityUser> manager,cajeroContext context)
        {
            _context = context;
            //inicializa el manager de usuarios
            _manager = manager;
        }

        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }


        // GET: Pullouts
        public async Task<IActionResult> Index()
        {
            //obtiene instancia de usuario actual
            var user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var pullout = from pu in _context.Pullout
                          select pu;
            //aplica clausula where para filtrar purchases por propietario
            pullout = pullout.Where(pu => pu.Customerid == user.Id);

            //envia lista filtrada para su renderizacion en "index.cshtml"
            return View(await pullout.AsNoTracking().ToListAsync());

        }

        // GET: Pullouts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pullout = await _context.Pullout
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pullout == null)
            {
                return NotFound();
            }

            return View(pullout);
        }

        // GET: Pullouts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Pullouts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Cash,Date,Customerid")] Pullout pullout)
        {

            var user = await GetCurrentUser();
            if (ModelState.IsValid)
            {

                //enlaza orden con usuario actual
                pullout.Customerid = user.Id;
                //setea fecha 
                pullout.Date = DateTime.Now;

                _context.Add(pullout);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(pullout);
        }

        // GET: Pullouts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pullout = await _context.Pullout.FindAsync(id);
            if (pullout == null)
            {
                return NotFound();
            }
            return View(pullout);
        }

        // POST: Pullouts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Cash,Date,Customerid")] Pullout pullout)
        {
            if (id != pullout.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pullout);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PulloutExists(pullout.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(pullout);
        }

        // GET: Pullouts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pullout = await _context.Pullout
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pullout == null)
            {
                return NotFound();
            }

            return View(pullout);
        }

        // POST: Pullouts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pullout = await _context.Pullout.FindAsync(id);
            _context.Pullout.Remove(pullout);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PulloutExists(int id)
        {
            return _context.Pullout.Any(e => e.Id == id);
        }
    }
}
