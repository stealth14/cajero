﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cajero.Models;

//herramientas del usuario logeado
using Microsoft.AspNetCore.Identity;
using cajero.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace cajero.Controllers
{
    [Authorize]
    public class DepositsController : Controller
    {
        private readonly cajeroContext _context;

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Deposit> Deposit { get; set; }
        public virtual DbSet<Pullout> Pullout { get; set; }
        public virtual DbSet<Transference> Transference { get; set; }

        //enlace con usuario
        private readonly UserManager<IdentityUser> _manager;

        public DepositsController(UserManager<IdentityUser> manager, cajeroContext context)
        {
            _context = context;
            //inicializa el manager de usuarios
            _manager = manager;
        }

        //obtener usuario actual
        private async Task<IdentityUser> GetCurrentUser()
        {
            return await _manager.GetUserAsync(HttpContext.User);
        }
        // GET: Deposits
        public async Task<IActionResult> Index()
        {

            //obtiene instancia de usuario actual
            var user = await GetCurrentUser();
            //itera y referencia purchases existentes en base de datos
            var deposit = from d in _context.Deposit
                            select d;
            //aplica clausula where para filtrar purchases por propietario
            deposit = deposit.Where(d => d.Customerid == user.Id);

            //envia lista filtrada para su renderizacion en "index.cshtml"
            return View(await deposit.AsNoTracking().ToListAsync());

        }

        // GET: Deposits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deposit = await _context.Deposit
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deposit == null)
            {
                return NotFound();
            }

            return View(deposit);
        }

        // GET: Deposits/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Deposits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Cash,Date,Customerid")] Deposit deposit)
        {
            var user = await GetCurrentUser();
            if (ModelState.IsValid)
            {
                //enlaza orden con usuario actual
                deposit.Customerid = user.Id;
                //setea fecha 
                deposit.Date = DateTime.Now;

                _context.Add(deposit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(deposit);
        }

        // GET: Deposits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deposit = await _context.Deposit.FindAsync(id);
            if (deposit == null)
            {
                return NotFound();
            }
            return View(deposit);
        }

        // POST: Deposits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Cash,Date,Customerid")] Deposit deposit)
        {
            if (id != deposit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deposit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepositExists(deposit.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(deposit);
        }

        // GET: Deposits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deposit = await _context.Deposit
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deposit == null)
            {
                return NotFound();
            }

            return View(deposit);
        }

        // POST: Deposits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deposit = await _context.Deposit.FindAsync(id);
            _context.Deposit.Remove(deposit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepositExists(int id)
        {
            return _context.Deposit.Any(e => e.Id == id);
        }
    }
}
