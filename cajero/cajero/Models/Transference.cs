﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace cajero.Models
{
    public partial class Transference
    {
        


        public int Id { get; set; }

        [Display(Name = "Monto")]
        [Required(ErrorMessage = "Campo Obligatorio")]
        public float? Cash { get; set; }

        [Display(Name = "Fecha")]
        public DateTime? Date { get; set; }

        public string Depositorid { get; set; }


        [Display(Name = "Nro Cuenta del Beneficiario")]
        [Required(ErrorMessage = "Campo Obligatorio")]
        public string Depositaryid { get; set; }
    }
}
