﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace cajero.Models
{
    public partial class Account
    {
        public int Id { get; set; }

        [Display(Name = "Saldo")]
        public float? Balance { get; set; }

        [Display(Name = "Nro Cuenta")]
        public string Code { get; set; }

        public string Customerid { get; set; }
    }
}
