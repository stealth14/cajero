﻿using System;
using System.Collections.Generic;

namespace cajero.Models
{
    public partial class Pullout
    {
        public int Id { get; set; }
        public float? Cash { get; set; }
        public DateTime? Date { get; set; }
        public string Customerid { get; set; }
    }
}
