﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace cajero.Models
{
    public partial class cajeroContext : DbContext
    {
        public cajeroContext()
        {
        }

        public cajeroContext(DbContextOptions<cajeroContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Pullout> Pullout { get; set; }
        public virtual DbSet<Deposit> Deposit { get; set; }
        public virtual DbSet<Transference> Transference { get; set; }
        public virtual DbSet<Account> Account { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-8N9QUA0; Initial Catalog=cajero; User Id=ronny; Password=naty");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pullout>(entity =>
            {
                entity.ToTable("pullout");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cash).HasColumnName("cash");

                entity.Property(e => e.Customerid)
                    .HasColumnName("customerid")
                    .HasMaxLength(450);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Transference>(entity =>
            {
                entity.ToTable("transference");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cash).HasColumnName("cash");

                entity.Property(e => e.Depositaryid)
                    .HasColumnName("depositaryid")
                    .HasMaxLength(450);

                entity.Property(e => e.Depositorid)
                .HasColumnName("depositorid")
                .HasMaxLength(450);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Deposit>(entity =>
            {
                entity.ToTable("deposit");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cash).HasColumnName("cash");

                entity.Property(e => e.Customerid)
                    .HasColumnName("customerid")
                    .HasMaxLength(450);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("account");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Balance).HasColumnName("balance");

                entity.Property(e => e.Customerid)
                    .HasColumnName("customerid")
                    .HasMaxLength(450);


            });
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
