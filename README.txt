Ronny Cajas
Xavier Calle
Anderson Cordova

Arquitectura

Framework Asp Net Core MvC

ISS en la parte del servidor
Bootstrap en la parte del cliente.


Patron de disenio: MVC


Funciones Principales

 - Registrio de Depositos
 - Recalculo de saldos de usuarios.
 - Permite consultar informacion de usuario.
 - Los usuarios solo pueden interactuar con los registros asociados a su cuenta de Asp Net Core.

Funciones:

Manejo de excepciones

- No permite gastar mas de lo que el usuario dispone en su cuenta.
- Captura errores y muestra una vista acorde.

Autorizacion.

- Valida que solo los usuarios autenticados puedan utilizar el sistema.
- Utiliza decorator [Authorized]

Autenticación

- Con herramientas Identity de Asp Net Core. 

Controladores Principales:

Account -> Para registro de cuentas bancarias.

Transfer -> Para las tranferencias a cuentas de otros usuarios

Deposit -> Para depositos en cuenta propia.


Vistas:

Area Principal.
Formularios para cada Controlador


Descripcion mas detallada de cada controlador


Controlador de despositos

Se crea una clausula tipo where para filtrar los depositos de cada usuario 
Se enlaza cada usuario creado con una cuenta tomando de igual manera la fecha actual.
Se muestra una vista con el historial de depositos por fecha. 
Para realizar un nuevo deposito es necesario ingresar el monto y presionar el boton depositar.

Controlador de retiros

Se crea una clausula tipo where para filtrar los retiros de cada usuario 
Se enlaza cada usuario creado con una cuenta tomando de igual manera la fecha actual.
Se muestra una vista con el historial de retiros por fecha. 
Para realizar un nuevo retiro es necesario ingresar el monto y presionar el boton retirar.

Controlador de cuentas

Se crea un enlace para el usuario con su respectiva cuenta.
Se crea muestra la informacion del numero de cuenta y el saldo de cada usuario
obeteniendo los datos del SGBD.

